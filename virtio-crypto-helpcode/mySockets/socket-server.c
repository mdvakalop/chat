/*
 * socket-server.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
 #include <sys/select.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"


/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt; //returns how many bytes where written
}


int main(void)
{
	char buf[1000]; 
	char addrstr[INET_ADDRSTRLEN];
	int sd, newsd, closed = 0;
	ssize_t n;
	socklen_t len;
	struct sockaddr_in sa;
	
	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
/*The  memset(void *s, int c, size_t n)  function  fills  the  first  n  bytes of the memory area pointed to by s with the constant byte c. Retruns pointer to the area, s */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(TCP_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY); //mikos analoga me socket?

	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) { //backlog: maximum length to which the queue of pending connections for sockfd. After backlog pendings -> reject
		perror("listen");
		exit(1);
	}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	fprintf(stderr, "Waiting for an incoming connection...\n");

	/* Accept an incoming connection */
	len = sizeof(struct sockaddr_in);
	if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
		perror("accept");
		exit(1);
	}
	if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) { //convert IPv4 and IPv6 addresses from binary to text form
		perror("could not format IP address");
		exit(1);
	}
	fprintf(stderr, "Incoming connection from %s:%d\n",
		addrstr, ntohs(sa.sin_port));

	/* We break out of the loop when the remote peer goes away */

	fd_set chat_set, copy_chat_set;
	FD_ZERO(&chat_set);
	FD_SET(0, &chat_set); //added server in chat_set
	FD_SET(newsd, &chat_set); //added client

	while(!closed) {

		copy_chat_set = chat_set;

		if (select(newsd + 1, &copy_chat_set, NULL, NULL, NULL) < 0) {
			perror("select");
			exit(1);
		}

		if (FD_ISSET(0, &copy_chat_set)) {//is server (stdin)

			n = read(0, buf, sizeof(buf)); //read from stdin
			if (n <= 0) {
				if (n < 0)
					perror("read from stdin (server) failed");
				else
					fprintf(stderr, "Peer went away\n");
				closed = 1;
				break;
			}
			if (insist_write(newsd, buf, n) != n) {
				perror("write to client failed");
				closed = 1;
				break;
			}
		}	
		else if (FD_ISSET(newsd, &copy_chat_set)) { //is client (newsd)

			fprintf(stderr, "Client: ");
			n = read(newsd, buf, sizeof(buf));
			if (n <= 0) {
				if (n < 0)
					perror("read from newsd (client) failed");
				else
					fprintf(stderr, "Peer went away\n");
				closed = 1;
				break;
			}
			if (insist_write(1, buf, n) != n) {
				perror("write to client failed");
				closed = 1;
				break;
			}
		}	
	}

	if (close(sd) < 0)
		perror("close");

	/* This will never happen */
	return 1;
}

