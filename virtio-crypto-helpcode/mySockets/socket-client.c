/*
 * socket-client.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

int main(int argc, char *argv[])
{
	int sd, port;
	ssize_t n, closed = 0;
	char buf[1000];
	char *hostname;
	struct hostent *hp;
	struct sockaddr_in sa;

	if (argc != 3) {
		fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
		exit(1);
	}
	hostname = argv[1];
	port = atoi(argv[2]); /* Needs better error checking */
/*behavior is the same as strtol(nptr, NULL, 10) except that atoi() does not detect errors.*/

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");
	
	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");



////////////////////////////////////////////////////////////////////////////////////////////

	fd_set chat_set, copy_chat_set;
	FD_ZERO(&chat_set);
	FD_SET(0, &chat_set); //added client in chat_set
	FD_SET(sd, &chat_set); //added server

	while(!closed) {
	
		copy_chat_set = chat_set;	

		if (select(sd + 1, &copy_chat_set, NULL, NULL, NULL) < 0) {
			perror("select");
			exit(1);
		}
		if (FD_ISSET(0, &copy_chat_set)) { //is client (stdin)

			n = read(0, buf, sizeof(buf)); //read from stdin
			if (n <= 0) {
				if (n < 0)
					perror("read from stdin (server) failed");
				else
					fprintf(stderr, "Peer went away\n");
				closed = 1;
				break;
			}
			if (insist_write(sd, buf, n) != n) {
				perror("write to server failed");
				closed = 1;
				break;
			}
		}
		else if (FD_ISSET(sd, &copy_chat_set)) { //is server (sd)
			
			fprintf(stderr, "Server: ");
			n = read(sd, buf, sizeof(buf));
			if (n <= 0) {
				if (n < 0)
					perror("read from sd (server) failed");
				else
					fprintf(stderr, "I went away\n");
				closed = 1;
				break;
			}
			if (insist_write(1, buf, n) != n) {
				perror("write to client failed");
				closed = 1;
				break;
			}
		}	
	}

	if (close(sd) < 0)	//in case server finishes (end of file)
		perror("close");

	/* This will never happen */
	return 1;
}
