/*
 * Virtio Crypto Device
 *
 * Implementation of virtio-crypto qemu backend device.
 *
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr> 
 *
 */

#include <qemu/iov.h>
#include "hw/virtio/virtio-serial.h"
#include "hw/virtio/virtio-crypto.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <crypto/cryptodev.h>

static uint32_t get_features(VirtIODevice *vdev, uint32_t features)
{
	DEBUG_IN();
	return features;
}

static void get_config(VirtIODevice *vdev, uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_config(VirtIODevice *vdev, const uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_status(VirtIODevice *vdev, uint8_t status)
{
	DEBUG_IN();
}

static void vser_reset(VirtIODevice *vdev)
{
	DEBUG_IN();
}


static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq)
//Η συνάρτηση vq_handle_output είναι η συνάρτηση που κα-
//λείται όταν ο frontend οδηγός έχει προσθέσει ένα buffer στην ουρά.
{
    VirtQueueElement elem;
    unsigned int *syscall_type, *ioctl_cmd;
    int  *host_fd, *host_ret_val,*ses_id;
    DEBUG_IN();

    if (!virtqueue_pop(vq, &elem)) {
        DEBUG("No item to pop from VQ");
        return;
    }

    DEBUG("I have got an item from VQ");

    syscall_type = elem.out_sg[0].iov_base;
    switch (*syscall_type) {

    case VIRTIO_CRYPTO_SYSCALL_TYPE_OPEN: //sg[2], syscall_type (R), host_fd (W)
        DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_OPEN");

        host_fd = elem.in_sg[0].iov_base;
        *host_fd = open("/dev/crypto", O_RDWR);

        break;

    case VIRTIO_CRYPTO_SYSCALL_TYPE_CLOSE: //sg[2], syscall_type (R), host_fd (R)
        DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_CLOSE");

        host_fd = elem.out_sg[1].iov_base;
        close(*host_fd);

        break;

    case VIRTIO_CRYPTO_SYSCALL_TYPE_IOCTL: //sgs: syscall_type (R), host_fd (R)
        DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_IOCTL");

        host_fd = elem.out_sg[1].iov_base;
        ioctl_cmd = elem.out_sg[2].iov_base;

        switch (*ioctl_cmd) {

        case CIOCGSESSION: //sgs: session (R), key (R), host_ret_val (W)
            DEBUG("In CIOCGSESSION!");

            struct session_op *session;
            session = elem.in_sg[0].iov_base;

            session->key = elem.out_sg[3].iov_base;

            host_ret_val = elem.in_sg[1].iov_base;
            *host_ret_val = ioctl(*host_fd, *ioctl_cmd, session);

            break;

        case CIOCFSESSION: //sgs: ses_id (R), host_ret_val (W)
            DEBUG("In CIOCFSESSION!");

            ses_id = elem.out_sg[3].iov_base;

            host_ret_val = elem.in_sg[1].iov_base;
            *host_ret_val = ioctl(*host_fd, *ioctl_cmd, ses_id);

            //ses_id = elem.out_sg[3].iov_base;

            break;

        case CIOCCRYPT:
            DEBUG("In CIOCCRYPT!"); //sgs: crypt(R), src(R), iv(R), dst(W), host_ret_val (W)

            struct crypt_op *crypt;
            crypt = elem.out_sg[3].iov_base;
            crypt->src = elem.out_sg[4].iov_base;
            crypt->iv = elem.out_sg[5].iov_base;
            crypt->dst = elem.in_sg[0].iov_base;

            host_ret_val = elem.in_sg[1].iov_base;
            *host_ret_val = ioctl(*host_fd, *ioctl_cmd, crypt);

            break;

        default:
            DEBUG("Unsupported ioctl command");

            break;

            }

        break;

    default:
        DEBUG("Unknown syscall_type");
    }

    virtqueue_push(vq, &elem, 0);
    virtio_notify(vdev, vq);
    printf("[VIRTIO-CRYPTO] [EXITING vq_handle_output]\n");
}




//arxikopoihsh odhgou.QQQ: to oti eexi anagnwristei h syskeyh (otan trexw to ./utopia ...) 
//shmainei oti exei ektelestei ayth edw katw (kai ara exei arxikopoih8ei kai to virtqueue)?
static void virtio_crypto_realize(DeviceState *dev, Error **errp)
{
    VirtIODevice *vdev = VIRTIO_DEVICE(dev);

    DEBUG_IN();

    virtio_init(vdev, "virtio-crypto", 13, 0);
    //συνδεει συσκευη στο bus (pci logika)
    virtio_add_queue(vdev, 128, vq_handle_output);
    //δημιο�~E�~Aγεί μία virtqueue και �~Dην �~@�~Aο�~Cθέ�~Dει �~C�~Dη λί�~C�~Dα με �~Dι�~B virr
    //tqueues �~Dη�~B �~C�~E�~Cκε�~Eή�~B vdev .
}

static void virtio_crypto_unrealize(DeviceState *dev, Error **errp)
{
    DEBUG_IN();
}

static Property virtio_crypto_properties[] = {
    DEFINE_PROP_END_OF_LIST(),
};

static void virtio_crypto_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    VirtioDeviceClass *k = VIRTIO_DEVICE_CLASS(klass);

    DEBUG_IN();
    dc->props = virtio_crypto_properties;
    set_bit(DEVICE_CATEGORY_INPUT, dc->categories);

    k->realize = virtio_crypto_realize;
    k->unrealize = virtio_crypto_unrealize;
    k->get_features = get_features;
    k->get_config = get_config;
    k->set_config = set_config;
    k->set_status = set_status;
    k->reset = vser_reset;
}

static const TypeInfo virtio_crypto_info = {
    .name          = TYPE_VIRTIO_CRYPTO,
    .parent        = TYPE_VIRTIO_DEVICE,
    .instance_size = sizeof(VirtCrypto),
    .class_init    = virtio_crypto_class_init,
};

static void virtio_crypto_register_types(void)
{
    type_register_static(&virtio_crypto_info);
}

type_init(virtio_crypto_register_types)