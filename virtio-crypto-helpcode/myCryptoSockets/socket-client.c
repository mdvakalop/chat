/*
 * socket-client.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"


int main(int argc, char *argv[])
{
	int sd, port,fd;
	int n, slen, closed = 0;
	char buf[DATA_SIZE];
	char *hostname;
	struct hostent *hp;
	struct sockaddr_in sa;
	unsigned char decr[DATA_SIZE], encr[DATA_SIZE];

	fd = open("/dev/crypto", O_RDWR);
	if (fd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	if (argc != 3) {
		fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
		exit(1);
	}
	hostname =argv[1];	//"83.212.103.155";
	port =atoi( argv[2]); /* Needs better error checking */
/*behavior is the same as strtol(nptr, NULL, 10) except that atoi() does not detect errors.*/

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");
	
	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */

	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");



////////////////////////////////////////////////////////////////////////////////////////////

	fd_set chat_set, copy_chat_set;
	FD_ZERO(&chat_set);
	FD_SET(0, &chat_set); //added client in chat_set
	FD_SET(sd, &chat_set); //added server

	while(!closed) {
	
		copy_chat_set = chat_set;	

		if (select(sd + 1, &copy_chat_set, NULL, NULL, NULL) < 0) {
			perror("select");
			exit(1);
		}
		if (FD_ISSET(0, &copy_chat_set)) { //is client (stdin)

           	memset(buf, 0, sizeof(buf));	//clean buf
			n = read(0, buf + prefix_size, sizeof(buf) - prefix_size); //read from stdin
			memcpy(buf, &n, prefix_size);
			if (n <= 0) {
				if (n < 0)
					perror("read from stdin (server) failed");
				else
					fprintf(stderr, "Peer went away\n");
				closed = 1;
				break;
			}

         	my_encrypt(fd, buf, encr);
			if (insist_write(sd, encr, DATA_SIZE) != DATA_SIZE) {
				perror("write to server from client failed");
				closed = 1;
				break;
			}
		}
		else if (FD_ISSET(sd, &copy_chat_set)) { //is server (sd)

			fprintf(stderr, "Server: ");
            memset(buf, 0, sizeof(buf));
			n = read(sd, buf, DATA_SIZE);
			if (n <= 0) {
				if (n < 0)
					perror("read from sd (server) failed");
				else
					fprintf(stderr, "Peer went away\n");
				/* Make sure we don't leak open files */
				if (close(sd) < 0)
					perror("close");
				closed = 1;
				break;
			}

           	my_decrypt(fd, buf, decr);
           	slen = *decr;
			if (insist_write(1, decr + prefix_size, slen) != slen) {
				perror("write to client from server failed");
				closed = 1;
				break;
			}
		}	
	}
	if (close(fd) < 0) {
		perror("close(fd)");
		return 1;
	}
	/* This will never happen */
	return 0;
}
