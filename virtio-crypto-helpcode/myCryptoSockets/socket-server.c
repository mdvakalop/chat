/*
 * socket-server.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
 #include <sys/select.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"


int main(void)
{   
	char buf[DATA_SIZE]; //bytes from client (used in read)
	char addrstr[INET_ADDRSTRLEN];
	int sd, newsd,fd;
	int n, closed = 0, slen = 0;
	socklen_t len;
	struct sockaddr_in sa;
	unsigned char encr[DATA_SIZE], decr[DATA_SIZE];
	
	fd = open("/dev/crypto", O_RDWR);
	if (fd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}
	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
/*The  memset(void *s, int c, size_t n)  function  fills  the  first  n  bytes of the memory area pointed to by s with the constant byte c. Retruns pointer to the area, s */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(TCP_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY); //mikos analoga me socket?

	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) { //backlog: maximum length to which the queue of pending connections for sockfd. After backlog pendings -> reject
		perror("listen");
		exit(1);
	}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	fprintf(stderr, "Waiting for an incoming connection...\n");

	/* Accept an incoming connection */
	len = sizeof(struct sockaddr_in);
	if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
		perror("accept");
		exit(1);
	}
	if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) { //convert IPv4 and IPv6 addresses from binary to text form
		perror("could not format IP address");
		exit(1);
	}
	fprintf(stderr, "Incoming connection from %s:%d\n",
		addrstr, ntohs(sa.sin_port));

	/* We break out of the loop when the remote peer goes away */

	fd_set chat_set, copy_chat_set;
	FD_ZERO(&chat_set);
	FD_SET(0, &chat_set); //added server in chat_set
	FD_SET(newsd, &chat_set); //added client

	while(!closed) {

		copy_chat_set = chat_set;	

		if (select(newsd + 1, &copy_chat_set, NULL, NULL, NULL) < 0) {  //why use select?
			perror("select");
			exit(1);
		}

		if (FD_ISSET(0, &copy_chat_set)) { //is server (stdin)

            memset(buf, 0, sizeof(buf));   	//clean buf
			n = read(0, buf + prefix_size, sizeof(buf) - prefix_size); //read from stdin. leave space (4 first bytes) for the int-len of string-input
			memcpy(buf, &n, prefix_size);
			if (n <= 0) {
				if (n < 0)
					perror("read from stdin (server) failed");
				else
					fprintf(stderr, "Peer went away\n");
				closed = 1;
				break;
			}

            my_encrypt(fd, buf, encr); 
  			//we transfer the whole array (DATA_SIZE size) to the other user
			if (insist_write(newsd, encr, DATA_SIZE) != DATA_SIZE) {
				perror("write to server fd=000 failed");
				closed = 1;
				break;
			}
		}	
		else if (FD_ISSET(newsd, &copy_chat_set)) { //is client (newsd)

			fprintf(stderr, "Client: ");
       		memset(buf, 0, sizeof(buf));	//clean buf
			n = read(newsd, buf, DATA_SIZE);
			if (n <= 0) {
				if (n < 0)
					perror("read from newsd (client) failed");
				else
					fprintf(stderr, "Peer went away\n");
				/* Make sure we don't leak open files */
				if (close(newsd) < 0)
					perror("close");
				closed = 1;
				break;
			}
            
            my_decrypt(fd, buf, decr); 
			slen = *decr;
			if (insist_write(1, decr + prefix_size, slen) != slen) {
				perror("write to server fd failed");
				closed = 1;
				break;
			}
		}	
	}

    	if (close(fd) < 0) {
		perror("close(fd)");
		return 1;
	}
	shutdown(sd, SHUT_WR);

	/* This will never happen */
	return 1;
}

