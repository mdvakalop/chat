/*
 * socket-common.h
 *
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#ifndef _SOCKET_COMMON_H
#define _SOCKET_COMMON_H
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <crypto/cryptodev.h>

#define DATA_SIZE       1024
#define BLOCK_SIZE      16
#define KEY_SIZE	16

/* Compile-time options */
#define TCP_PORT    8080
#define TCP_BACKLOG 5

#define HELLO_THERE "Hello there!"

int prefix_size = sizeof(int);


ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt; //returns how many bytes where written
}



static int my_decrypt(int cfd, unsigned char *myData, unsigned char *decr)
{
	unsigned char *key = "2746924", *iv = "33333";
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	decrypted[DATA_SIZE],
				encrypted[DATA_SIZE];
	} data;
	
	memset(decr, 0, DATA_SIZE);
	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	memset(&data, 0, sizeof(data));

	memcpy(data.encrypted, myData, DATA_SIZE);

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;		//why strlen(key) does not work?
	sess.key = key;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		exit(2);
	}

	/*
	 * Encrypt data.in to data.encrypted
	 */
	cryp.ses = sess.ses;
	cryp.len = DATA_SIZE;
	cryp.src = data.encrypted;
	cryp.dst = data.decrypted;
	cryp.iv = iv;
	cryp.op = COP_DECRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		exit(2);
	}
    	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
        	exit(2);
	}

	memcpy(decr, data.decrypted, DATA_SIZE);

	return 0;

}



static int my_encrypt(int cfd, unsigned char *myData, unsigned char *encr)
{
    unsigned char *key = "2746924", *iv = "33333";
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE];
	} data;

	memset(encr, 0, DATA_SIZE);
	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	memset(&data, 0, sizeof(data));
	
	memcpy(data.in, myData, DATA_SIZE);
	//strcpy(data.in, myData+4); //ayto tha perepe na paizei!!!!!!!!!!!!!!!! giati to myData einaikanoiko string. doulecei alla thereitai str kai to int
	
	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = key;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		exit(1);
	}

	/*
	 * Encrypt data.in to data.encrypted
	 */
	cryp.ses = sess.ses;
	cryp.len = DATA_SIZE;
	cryp.src = data.in;
	cryp.dst = data.encrypted;
	cryp.iv = iv;
	cryp.op = COP_ENCRYPT;


	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		exit(1);
	}

     	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
  	}

	memcpy(encr, data.encrypted, DATA_SIZE);

    return 0;
}



#endif /* _SOCKET_COMMON_H */

