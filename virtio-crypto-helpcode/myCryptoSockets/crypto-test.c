/*
 * test_crypto.c
 * 
 * Performs a simple encryption-decryption 
 * of random data from /dev/urandom with the 
 * use of the cryptodev device.
 *
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <crypto/cryptodev.h>

#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE	16  /* AES128 */
//#define COMMON_KEY  "921364928"
//uint8_t COMMON_IV[DATA_SIZE]='e744564de53dc8ef2783194a4152d74';
//unsigned char COMMON_KEY[DATA_SIZE]='5b535cfba4c8d1cac9bc9b237848829';



//struct crypt_op {
	//__u32	ses;		/* session identifier */
	//__u16	op;		/* COP_ENCRYPT or COP_DECRYPT */
	//__u16	flags;		/* see COP_FLAG_* */
	//__u32	len;		/* length of source data */
	//__u8	__user *src;	/* source data */
	//__u8	__user *dst;	/* pointer to output data */
	/* pointer to output data for hash/MAC operations */
	//__u8	__user *mac;
	/* initialization vector for encryption operations */
	//__u8	__user *iv;
//};

/* Insist until all of the data has been read */
ssize_t insist_read(int fd, void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = read(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

static int fill_urandom_buf(unsigned char *buf, size_t cnt)
{
        int crypto_fd;
        int ret = -1;

        crypto_fd = open("/dev/urandom", O_RDONLY);
        if (crypto_fd < 0)
                return crypto_fd;

        ret = insist_read(crypto_fd, buf, cnt);
        close(crypto_fd);

        return ret;
}


static int my_decrypt(int cfd,unsigned char *myData)
{
    uint8_t iv[BLOCK_SIZE];
	uint8_t key[KEY_SIZE];
    memset(key, 0x33,  sizeof(key));
	memset(iv, 0x03,  sizeof(iv));

	int i = -1, myDataLen = 0;
	//unsigned char *key = COMMON_KEY, *iv = COMMON_IV;
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	} data;

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	
	strcpy(data.in, myData);
	myDataLen = strlen(myData);

	//strcpy(data.key, key);
	//strcpy(data.iv, iv);

	//printf("\nOriginal data:\n");
printf("arxiko=    %s \n",data.in);
/*	for (i = 0; i < myData; i++)
		printf("%c", data.in[i]);
	printf("\n");*/

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = key;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}

	/*
	 * Encrypt data.in to data.encrypted
	 */
	cryp.ses = sess.ses;
	cryp.len =sizeof(data.in); //myDataLen;
	cryp.src = data.in;
	cryp.dst = data.encrypted;
	cryp.iv = iv;
	cryp.op = COP_DECRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}

	printf("\nddddddddddddEncrypted data:\n");
	for (i = 0; i < 50; i++) {
		printf("%c", data.encrypted[i]);
	}
	printf("\n");

	return 0;
}

static int my_encrypt(int cfd,unsigned char *myData)
{
    uint8_t iv[BLOCK_SIZE];
	uint8_t key[KEY_SIZE];
    memset(key, 0x33,  sizeof(key));
	memset(iv, 0x03,  sizeof(iv));

	int i = -1, myDataLen = 0;
	//unsigned char *key = COMMON_KEY, *iv = COMMON_IV;
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	} data;

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	
	strcpy(data.in, myData);
	myDataLen = strlen(myData);

	//strcpy(data.key, key);
	//strcpy(data.iv, iv);

	//printf("\nOriginal data:\n");
printf("arxiko=    %s \n",data.in);
	for (i = 0; i < myDataLen; i++)
		printf("%c", data.in[i]);
	printf("\n");

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = key;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}

	/*
	 * Encrypt data.in to data.encrypted
	 */
	cryp.ses = sess.ses;
	cryp.len =sizeof(data.in); //myDataLen;
	cryp.src = data.in;
	cryp.dst = data.encrypted;
	cryp.iv = iv;
	cryp.op = COP_ENCRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}

	printf("\nEncrypted data:\n");
	for (i = 0; i < DATA_SIZE; i++) {
		printf("%c", data.encrypted[i]);
	}
	printf("\n");
my_decrypt(cfd,data.encrypted);

	return 0;
}








































int main(void)
{
	int fd;

	fd = open("/dev/crypto", O_RDWR);
	if (fd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}
uint8_t *k="helll000000000000000000000000000000000000000000000000000000000000000000000000000000o";

	if (my_encrypt(fd, k) < 0) {
		return 1;
	}

	if (close(fd) < 0) {
		perror("close(fd)");
		return 1;
	}

	return 0;
}
