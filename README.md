Development of chat application for remote servers over TCP/IP using sockets, cryptographic device VirtIO, QEMU and KVM.
A project for Laboratory of operating systems course at NTUA ECE. <br/>
Developed by [Myrsini Vakalopoulou ](https://gitlab.com/mdvakalop) and [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous)